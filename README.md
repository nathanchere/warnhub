# ![Warnhub](assets/logo_300.png)

The primary goals of Warnhub are:

* clear visibility of production problems as early as possible

Secondary / 'nice to have' goals are:

* reassurance to both technical and non-technical stakekholders about the health of an IT system
* create a 'pit of success' for maintaining documentation for complex infrastucture

## Changelog

### v2020-07-10

* Placeholder. Currently still very much pre-alpha.

## Roadmap

* TODO

## Contributing

There is no desire for public contributions at this point. Any future participation in this project will be subject to the [docs/CODE_OF_CODING.md](Code of Coding).

## License

Source code is provided under [GNU Affero General Public License v3](https://www.gnu.org/licenses/agpl-3.0.en.html)